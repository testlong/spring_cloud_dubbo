package com.test.demo.dubbo;

import com.test.demo.dubbo.api.MyApi;
import com.test.demo.dubbo.api.MyFeignApi;
import com.test.demo.dubbo.api.request.MyRequest;
import com.test.demo.dubbo.api.response.MyResponse;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * @author laigl
 * @ClassName
 * @Description
 * @Date 2020/02/18 12:21
 */
//消费者端，开启@FeignClient
@EnableFeignClients(basePackages = "com.test.demo")
@SpringBootApplication
@EnableDiscoveryClient
@RestController
public class ConsumerApp {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerApp.class, args);
    }

    @Reference
    private MyApi myApi;

    @Autowired
    private MyFeignApi myFeignApi;

    @Autowired
    private RequestMappingHandlerMapping requestMappingHandlerMapping;

    @GetMapping("test1")
    public MyResponse test1() {
        MyRequest req = new MyRequest();
        req.setReqTime(System.currentTimeMillis());
        return myApi.getTime(req);
    }


    @GetMapping("test2")
    public MyResponse test2() {
        MyRequest req = new MyRequest();
        req.setReqTime(System.currentTimeMillis());
        return myFeignApi.getTime(req);
    }

    @GetMapping("add")
    public void add() throws Exception {
        RequestMappingInfo requestMappingInfo = RequestMappingInfo
                .paths("/addone")
                .methods(RequestMethod.POST)
                .consumes(MediaType.APPLICATION_JSON_VALUE)
                .build();
        // 这里就注册了我们的handlermapping，但是这里只能一个一个方法进行注册（而且不限制你重复注册，但是如果重复注册的话，请求的时候会报错）
        requestMappingHandlerMapping.registerMapping(requestMappingInfo, myFeignApi, MyFeignApi.class.getDeclaredMethod("getTime", MyRequest.class));
    }

}

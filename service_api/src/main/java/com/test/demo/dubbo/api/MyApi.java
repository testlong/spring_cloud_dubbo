package com.test.demo.dubbo.api;

import com.test.demo.dubbo.api.request.MyRequest;
import com.test.demo.dubbo.api.response.MyResponse;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author laigl
 * @ClassName
 * @Description
 * @Date 2020/02/20 11:53
 */
public interface MyApi {
    @ResponseBody
    MyResponse getTime(MyRequest req);

}

package com.test.demo.dubbo.api.response;

import java.io.Serializable;
import java.util.Date;

/**
 * @author laigl
 * @ClassName
 * @Description
 * @Date 2020/02/20 12:08
 */
public class MyResponse implements Serializable {

    private Long respTime;

    private Date date;

    public Long getRespTime() {
        return respTime;
    }

    public void setRespTime(Long respTime) {
        this.respTime = respTime;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}

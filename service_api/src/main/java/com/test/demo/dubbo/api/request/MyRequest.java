package com.test.demo.dubbo.api.request;

import java.io.Serializable;
import java.util.Date;

/**
 * @author laigl
 * @ClassName
 * @Description
 * @Date 2020/02/20 12:08
 */
public class MyRequest implements Serializable {

    private Long reqTime;

    public Long getReqTime() {
        return reqTime;
    }

    public void setReqTime(Long reqTime) {
        this.reqTime = reqTime;
    }

}

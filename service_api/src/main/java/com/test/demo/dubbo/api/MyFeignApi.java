package com.test.demo.dubbo.api;

import com.test.demo.dubbo.api.request.MyRequest;
import com.test.demo.dubbo.api.response.MyResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient("spring-cloud-alibaba-dubbo-server")
public interface MyFeignApi {

    @PostMapping("getTime")
    MyResponse getTime(@RequestBody MyRequest req);

}

package com.test.demo.dubbo;

import com.test.demo.dubbo.api.MyFeignApi;
import com.test.demo.dubbo.api.request.MyRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * @author laigl
 * @ClassName
 * @Description
 * @Date 2020/02/18 12:06
 */
@EnableDiscoveryClient
@SpringBootApplication
@RestController
public class ProviderApp {
    public static void main(String[] args) {
        SpringApplication.run(ProviderApp.class, args);
    }

    @Autowired
    private RequestMappingHandlerMapping requestMappingHandlerMapping;
    @Autowired
    private MyFeignApi myFeignApi;

    @GetMapping("add")
    public void add() throws Exception {
        RequestMappingInfo requestMappingInfo = RequestMappingInfo
                .paths("getTime")
                .methods(RequestMethod.POST)
                .build();
        // 这里就注册了我们的handlermapping，但是这里只能一个一个方法进行注册（而且不限制你重复注册，但是如果重复注册的话，请求的时候会报错）
        requestMappingHandlerMapping.registerMapping(requestMappingInfo, myFeignApi, MyFeignApi.class.getDeclaredMethod("getTime", MyRequest.class));
    }

}

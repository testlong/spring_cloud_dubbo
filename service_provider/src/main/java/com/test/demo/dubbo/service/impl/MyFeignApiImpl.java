package com.test.demo.dubbo.service.impl;

import com.test.demo.dubbo.api.MyFeignApi;
import com.test.demo.dubbo.api.request.MyRequest;
import com.test.demo.dubbo.api.response.MyResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class MyFeignApiImpl implements MyFeignApi {
    @Override
    public MyResponse getTime(MyRequest req) {
        System.out.println("请求参数reqTime为" + req.getReqTime());
        MyResponse resp = new MyResponse();
        resp.setRespTime(System.currentTimeMillis());
        resp.setDate(new Date());
        return resp;
    }
}

package com.test.demo.dubbo.service.impl;

import com.test.demo.dubbo.api.MyApi;
import com.test.demo.dubbo.api.request.MyRequest;
import com.test.demo.dubbo.api.response.MyResponse;
import org.apache.dubbo.config.annotation.Service;

import java.util.Date;

/**
 * @author laigl
 * @ClassName
 * @Description
 * @Date 2020/02/20 11:55
 */
@Service
public class MyApiImpl implements MyApi {
    @Override
    public MyResponse getTime(MyRequest req) {
        System.out.println("请求参数reqTime为" + req.getReqTime());
        MyResponse resp = new MyResponse();
        resp.setRespTime(System.currentTimeMillis());
        resp.setDate(new Date());
        return resp;
    }
}

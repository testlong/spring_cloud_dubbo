package com.test.demo.nacos.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author laigl
 * @ClassName
 * @Description
 * @Date 2020/02/19 10:21
 */
@SpringBootApplication
@RestController
@RefreshScope
public class NacosConfigApp {

    public static void main(String[] args) {
        SpringApplication.run(NacosConfigApp.class, args);
    }

    @Value("${my.title}")
    private String title;

    @GetMapping("test")
    public String test() {
        return "title = " + title;
    }

}
